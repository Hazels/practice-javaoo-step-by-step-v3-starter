# Daily Report (2023/07/12)
# O (Objective): 
## 1. What did we learn today?
### We learned code review,Java Stream API and Object-Oriented Programming today.

## 2. What activities did you do? 
### I had a code review with my team members to make sure the code was compliant and did some Java code exercises.

## 3. What scenes have impressed you?
### I have been impressed by the code review with my team members today .

---------------------

# R (Reflective): 
## Please use one word to express your feelings about today's class.
---------------------
### Dedicated.

# I (Interpretive):
## What do you think about this? What was the most meaningful aspect of this activity?
---------------------
### I think the most meaningful aspect of this activity is having code review with my team members,because I can learn others' excellent design ideas and business logic and deepen my understanding of requirements.What's more,it is an important part that team members review each other's code to validate requirements, find bugs, and point out "low-quality" code that doesn't conform to improve the code quality of the entire team.

# D (Decisional): 
## Where do you most want to apply what you have learned today? What changes will you make?
---------------------
### I will use Java stream instead of loop when I write the functions.
### I think I still need to preview before class in order to better keep up with the teacher's progress.





