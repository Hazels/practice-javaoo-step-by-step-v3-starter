package ooss;

import java.util.HashSet;
import java.util.Objects;

public class Klass implements Subject{
    private final int number;

    private Student student;

    public Klass(int number) {
        this.number = number;
    }

    private final HashSet<Teacher> teachers = new HashSet<>();
    private final HashSet<Student> students = new HashSet<>();
    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }


    //通知
    public void assignLeader(Student student) {
        this.student = student;
        if (student.getKlass() == null || student.getKlass().getNumber() != number)
            System.out.println("It is not one of us.");
        if (!students.isEmpty()){
            students.forEach(student1 -> {student1.update(this.number,student.name);});
        }
        if (!teachers.isEmpty()){
            teachers.forEach(teacher -> {teacher.update(this.number,student.name);});
        }
    }

    public boolean isLeader(Student student) {
        return this.student.equals(student);
    }


    //确定观察者
    public void attach(Student student) {
        students.add(student);
    }

    public void attach(Teacher teacher) {
        teachers.add(teacher);
    }

}
