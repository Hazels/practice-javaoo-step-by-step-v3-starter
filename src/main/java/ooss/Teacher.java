package ooss;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends Person implements Observer {
    private final List<Klass> klass = new ArrayList<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce(){
        StringBuilder stringBuilder = new StringBuilder("My name is "+this.name+". I am "+this.age+" years old. I am a teacher. ");

        if (! klass.isEmpty())
        {
            stringBuilder.append("I teach Class ");
            klass.forEach(item->{
            if (item.equals(klass.get(klass.size()-1)))
                stringBuilder.append(item.getNumber()).append(".");
            else stringBuilder.append(item.getNumber()).append(", ");
            });
        }
        return stringBuilder.toString();
    }

    public void assignTo(Klass klass) {
        this.klass.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klass.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return klass.contains(student.getKlass());
    }

    @Override
    public void update(int number,String leaderName) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.",this.name,number,leaderName);
    }

}
