package ooss;

public interface Observer {

        public void update(int number,String leaderName);
}
