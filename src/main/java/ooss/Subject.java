package ooss;

public interface Subject {
    public void attach(Student student);
    public void assignLeader(Student king);

    public void attach(Teacher teacher);
}
