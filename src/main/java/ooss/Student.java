package ooss;

public class Student extends Person implements Observer{

    private Klass klass;
    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    //Student class will add a klass attribute to indicate the class to which it belongs. Student class will provide a join method to change the class to which it belongs, and a isIn method to judge whether it belongs to the class.
    //
    //When belonging to a class, a student will introduce him/herself with class number like:
    //
    //My name is Tom. I am 18 years old. I am a student. I am in class 1.
    public void join(Klass klass){
        this.klass = klass;
    }

    public boolean isIn(Klass klass){
        return this.klass != null && this.klass.equals(klass);
    }

    public Klass getKlass() {
        return klass;
    }

    @Override
    public String introduce(){
        if (this.getKlass() != null && this.getKlass().isLeader(this))
            return String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.",this.name,this.age,this.klass.getNumber());
        else
            return "It is not one of us.";
//        return String.format("My name is %s. I am %d years old. I am a student. I am in class %d.",this.name,this.age,this.klass.getNumber());
    }

    @Override
    public void update(int number,String leaderName) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.",this.name,number,leaderName);
    }
}
